package com.xsis.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="fakultas")
public class FakultasModel {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "fakultas_seq")
	@TableGenerator(name = "fakultas_seq", table = "tbl_sequence", pkColumnName = "seq_id",
	valueColumnName = "seq_value", initialValue = 0, allocationSize = 1)
	private int id;
	
	@Column(name = "kd_fakultas", nullable = false)
	private String kdFakultas;
	
	@Column(name="nm_fakultas", nullable = false)
	private String nmFakultas;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKdFakultas() {
		return kdFakultas;
	}

	public void setKdFakultas(String kdFakultas) {
		this.kdFakultas = kdFakultas;
	}

	public String getNmfakultas() {
		return nmFakultas;
	}

	public void setNmFakultas(String nmFakultas) {
		this.nmFakultas = nmFakultas;
	}
}
