package com.xsis.api.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class JurusanController {
	@RequestMapping(value="/jurusan", method = RequestMethod.GET)
	public String index() {
		return "jurusan/index";
	}
	
	@RequestMapping(value="/jurusan/create", method = RequestMethod.GET)
	public String create() {
		return "jurusan/create";
	}

}
