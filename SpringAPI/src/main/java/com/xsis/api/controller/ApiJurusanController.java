package com.xsis.api.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.api.model.JurusanModel;
import com.xsis.api.repository.JurusanRepo;

@Controller
public class ApiJurusanController {
	@Autowired
	private JurusanRepo repo;
	
	private Log log = LogFactory.getLog(getClass());
	
	@RequestMapping(value="/api/jurusan/", method = RequestMethod.GET)
	public ResponseEntity<List<JurusanModel>> list(){
		ResponseEntity<List<JurusanModel>> hasil = null;
		try {
			List<JurusanModel> list = repo.findAll();
			hasil = new ResponseEntity<List<JurusanModel>>(list, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage(), e);
			hasil = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return hasil;
	}
	
	@RequestMapping(value = "/api/jurusan/", method = RequestMethod.POST)
	public ResponseEntity<JurusanModel> create(@RequestBody JurusanModel item){
		ResponseEntity<JurusanModel> hasil = null;
		try {
			repo.save(item);
			hasil = new ResponseEntity<JurusanModel>(item, HttpStatus.CREATED);
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage(), e);
			hasil = new ResponseEntity<JurusanModel>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return hasil;
	}
	
	@RequestMapping(value = "/api/jurusan/{id}")
	public ResponseEntity<JurusanModel> delete(@PathVariable(name = "id")Integer id){
		ResponseEntity<JurusanModel> hasil = null;
		try {
			JurusanModel item = repo.findById(id).orElse(null);
			repo.delete(item);
			hasil = new ResponseEntity<JurusanModel>(item, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage(), e);
			hasil = new ResponseEntity<JurusanModel>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return hasil;
	}

}
