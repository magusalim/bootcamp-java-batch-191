package day01;

public class If_Else_Condition {

	public static void main(String[] args) {

		int x = 12;
		
		if (x + 8 == 20) {
			System.out.println("If pertama berhasil");
		} else if (x == 4 * 3) {
			System.out.println("Else If kedua yang dapet");
		} else if (x == 12 / 1) {
			System.out.println("Else If ketiga yang dapet");
		}
		
		if (x % 7 == 5 ) {
			System.out.println("If kedua berhasil");
		}
		
	}

}
