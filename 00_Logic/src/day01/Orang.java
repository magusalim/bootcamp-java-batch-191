package day01;

public class Orang {
	
	// list property/state
	String nama;
	String alamat;
	String jenisKelamin;
	int umur;
	String tempatLahir;
	
	public void print() {
		System.out.println("Nama = "+ nama);
		System.out.println("Alamat = "+ alamat);
		System.out.println("Jenis Kelamin = "+ jenisKelamin);
		System.out.println("Umur = "+ umur);
		System.out.println("Tempat Lahir = "+ tempatLahir);
		System.out.println();
	}

}
