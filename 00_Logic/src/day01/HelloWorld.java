package day01;

public class HelloWorld {	

	public static void main(String[] args) {
		
		System.out.println("Welcome to Java");
		System.out.println();
		
		makananKesukaan();						//panggil method
		sampleObject();							//panggil method
		
	}
	
	public static void sampleObject() {
				
		Orang org1 = new Orang(); 				//instansiasi object
		
		org1.nama = "Agus"; 					//property
		org1.alamat = "Cipete Raya";			//property
		org1.jenisKelamin = "Pria";				//property
		org1.umur = 23;							//property
		org1.tempatLahir = "Jakarta"; 			//property
		org1.print();							//panggil method print dari class Orang
		
//		System.out.println("Nama = "+ org1.nama);
//		System.out.println("Alamat = "+ org1.alamat);
//		System.out.println("Jenis Kelamin = "+ org1.jenisKelamin);
//		System.out.println("Umur = "+ org1.umur);
//		System.out.println("Tempat Lahir = "+ org1.tempatLahir);
//		System.out.println();
				
		Orang org2 = new Orang();
		org2.nama = "Adi";
		org2.alamat = "Kemang Raya";
		org2.jenisKelamin = "Pria";
		org2.umur = 24;
		org2.tempatLahir = "Jakarta";
		org2.print();
		
//		System.out.println("Nama = "+ org2.nama);
//		System.out.println("Alamat = "+ org2.alamat);
//		System.out.println("Jenis Kelamin = "+ org2.jenisKelamin);
//		System.out.println("Umur = "+ org2.umur);
//		System.out.println("Tempat Lahir = "+ org2.tempatLahir);
//		System.out.println();
		
		Orang org3 = new Orang();
		org3.nama = "Agus";
		org3.alamat = "Cipete Raya";
		org3.jenisKelamin = "Pria";
		org3.umur = 23;
		org3.tempatLahir = "Jakarta";
		org3.print();
		
//		System.out.println("Nama = "+ org3.nama);
//		System.out.println("Alamat = "+ org3.alamat);
//		System.out.println("Jenis Kelamin = "+ org3.jenisKelamin);
//		System.out.println("Umur = "+ org3.umur);
//		System.out.println("Tempat Lahir = "+ org3.tempatLahir);
//		System.out.println();
				
		Orang org4 = new Orang();
		org4.nama = "Adi";
		org4.alamat = "Kemang Raya";
		org4.jenisKelamin = "Pria";
		org4.umur = 24;
		org4.tempatLahir = "Jakarta";
		org4.print();
		
//		System.out.println("Nama = "+ org4.nama);
//		System.out.println("Alamat = "+ org4.alamat);
//		System.out.println("Jenis Kelamin = "+ org4.jenisKelamin);
//		System.out.println("Umur = "+ org4.umur);
//		System.out.println("Tempat Lahir = "+ org4.tempatLahir);
//		System.out.println();
		
		Orang org5 = new Orang();
		org5.nama = "Adi";
		org5.alamat = "Kemang Raya";
		org5.jenisKelamin = "Pria";
		org5.umur = 24;
		org5.tempatLahir = "Jakarta";
		org5.print();
		
//		System.out.println("Nama = "+ org5.nama);
//		System.out.println("Alamat = "+ org5.alamat);
//		System.out.println("Jenis Kelamin = "+ org5.jenisKelamin);
//		System.out.println("Umur = "+ org5.umur);
//		System.out.println("Tempat Lahir = "+ org5.tempatLahir);
		
//		String nama1 = "Agus";				// variable saja
//		String jenisKelamin1 = "Pria";		// variable saja
//		int umur1 = 20;						// variable saja
//		
//		String nama2 = "Adi";
//		String jenisKelamin2 = "Pria";
//		int umur2 = 22;
	}
	
	public static void makananKesukaan() {
		System.out.println("1. Soto Madura");
		System.out.println("2. Bakso");
		System.out.println("3. Pizza");
		System.out.println("4. Nasi Ayam Sambel");
		System.out.println("5. Sate Ayam");
	}

}
