package day01;

public class TugasDay1 {

	public static void main(String[] args) {
		
		System.out.println("TUGAS DAY 01");
		System.out.println();
		
		ngulangKe1();
		System.out.println();
		
		ngulangKe2();
		System.out.println();
		
		ngulangKe3();
		System.out.println();
		
		ngulangKe4();
		System.out.println();
		
		ngulangKe5();
		System.out.println();
		
		ngulangKe6();
		System.out.println();
		
		ngulangKe7();
		System.out.println();

	}
	
	public static void ngulangKe1() {

		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ( Kerjaan Ke-1 )");
		System.out.println();
		
		System.out.println("Soal 01");
		int a = 1;
		for (int i = 0; i < 7; i++) {
			System.out.print(a + " ");
			a = a + 2;
		}
		System.out.println();System.out.println();
		
		
		System.out.println("Soal 02");
		int b = 2;
		for (int i = 0; i < 7; i++) {
			System.out.print(b + " ");
			b = b + 2;
		}
		System.out.println();System.out.println();
		
		
		System.out.println("Soal 03");
		int c = 1;
		for (int i = 0; i < 7; i++) {
			System.out.print(c + " ");
			c = c + 3;
		}
		System.out.println();System.out.println();
		
		
		System.out.println("Soal 04");
		int d = 1;
		for (int i = 0; i < 7; i++) {
			System.out.print(d + " ");
			d = d + 3;
		}
		System.out.println();System.out.println();
		
		
		System.out.println("Soal 05");
		int e = 1;
		for (int i = 0; i < 7; i++) {
			
			if (i == 2 || i == 5) {
				System.out.print("*" + " ");
			} else if (i != 2 || i != 5) {
				System.out.print(e + " ");
				e = e + 4;
			}
			
		}
		System.out.println();System.out.println();
		
		
		System.out.println("Soal 06");
		int f = 1;
		for (int i = 0; i < 7; i++) {
			
			if (i == 2 || i == 5) {
				System.out.print("*" + " ");
			} else if (i != 2 || i != 5) {
				System.out.print(f + " ");
				
			}
			f = f +4;
		}
		System.out.println();System.out.println();
		
		
		System.out.println("Soal 07");
		int g = 2;
		for (int i = 0; i < 7; i++) {
			System.out.print(g + " ");
			g = g * 2;
		}
		System.out.println();System.out.println();
		
		
		System.out.println("Soal 08");
		int h = 3;
		for (int i = 0; i < 7; i++) {
			System.out.print(h + " ");
			h = h * 3;
		}
		System.out.println();System.out.println();
		
		
		System.out.println("Soal 09");
		int j = 4;
		for (int i = 0; i < 7; i++) {
			
			if (i == 2 || i == 5) {
				System.out.print("*" + " ");
				
				
			} else if (i != 2 || i != 5) {
				System.out.print(j + " ");
				j = j * 4;
			}
			
			
		}
		System.out.println();System.out.println();
		
		
		System.out.println("Soal 10");
		int k = 3;
		for (int i = 0; i < 7; i++) {
			
			if (i != 3) {
				System.out.print(k + " ");
			} else if (i == 3) {
				System.out.print("XXX" + " ");
			}
			k = k * 3;
			
		}
		System.out.println();System.out.println();
		
	}
	
	public static void ngulangKe2() {
		
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ( Kerjaan Ke-2 )");
		System.out.println();
		
		int n = 7;
		
		System.out.println("Soal Ke-1");
		int a = 1;
		for (int i = 0; i < n; i++) {
			System.out.print(a + ",");
			a = a +2;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal Ke-2");
		int b = 2;
		for (int i = 0; i < n; i++) {
			System.out.print(b + ",");
			b = b +2;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal Ke-3");
		int c = 1;
		for (int i = 0; i < n; i++) {
			System.out.print(c + ",");
			c = c + 3;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal Ke-4");
		int d = 1;
		for (int i = 0; i < n; i++) {
			System.out.print(d + ",");
			d = d + 3;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal Ke-5");
		int e = 1;
		for (int i = 0; i < n; i++) {
			
			if (i == 2 || i == 5) {
				System.out.print("*,");
			} else {
				System.out.print(e + ",");
				e = e + 4;
			}
			
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal Ke-6");
		int f = 1;
		for (int i = 0; i < n; i++) {
			
			if (i == 2 || i == 5) {
				System.out.print("*,");
			} else {
				System.out.print(f + ",");
			}
			f = f + 4;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal Ke-7");
		int g = 2;
		for (int i = 0; i < n; i++) {
			System.out.print(g + ",");
			g = g * 2;		
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal Ke-8");
		int h = 3;
		for (int i = 0; i < n; i++) {
			System.out.print(h + ",");
			h = h *3;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal Ke-9");
		int j = 4;
		for (int i = 0; i < n; i++) {
			
			if (i == 2 || i == 5) {
				System.out.print("*,");
			} else {
				System.out.print(j + ",");
				j = j * 4;
			}
			
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal Ke-10");
		int k = 3;
		for (int i = 0; i < n; i++) {
			
			if (i == 3) {
				System.out.print("XXX,");
			} else {
				System.out.print(k + ",");
			}
			k = k * 3;
			
		}
		System.out.println();System.out.println();
		
	}
	
	public static void ngulangKe3() {
		
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ( Kerjaan Ke-3 )");
		System.out.println();
		
		int n = 7;
		
		System.out.println("Soal 1");
		int x1 = 1;
		for (int i = 0; i < n; i++) {
			System.out.print(x1 + " ");
			x1 = x1 + 2;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 2");
		int x2 = 2;
		for (int i = 0; i < n; i++) {
			System.out.print(x2 + " ");
			x2 = x2 + 2;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 3");
		int x3 = 1;
		for (int i = 0; i < n; i++) {
			System.out.print(x3 + " ");
			x3 = x3 + 3;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 4");
		int x4 = 1;
		for (int i = 0; i < n; i++) {
			System.out.print(x4 + " ");
			x4 = x4 + 3;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 5");
		int x5 = 1;
		for (int i = 0; i < n; i++) {
			
			if (i == 2 || i == 5) {
				System.out.print("* ");
			} else {
				System.out.print(x5 + " ");
				x5 = x5 + 4;
			}
			
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 6");
		int x6 = 1;
		for (int i = 0; i < n; i++) {
			
			if (i == 2 || i == 5) {
				System.out.print("* ");
			} else {
				System.out.print(x6 + " ");
			}
			x6 = x6 + 4;
			
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 7");
		int x7 = 2;
		for (int i = 0; i < n; i++) {
			System.out.print(x7 + " ");
			x7 = x7 * 2;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 8");
		int x8 = 3;
		for (int i = 0; i < n; i++) {
			System.out.print(x8 + " ");
			x8 = x8 * 3;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 9");
		int x9 = 4;
		for (int i = 0; i < n; i++) {
			
			if (i == 2 || i == 5) {
				System.out.print("* ");
			} else {
				System.out.print(x9 + " ");
				x9  = x9 * 4;
			}
			
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 10");
		int x10 = 3;
		for (int i = 0; i < n; i++) {
			
			if (i == 3) {
				System.out.print("XXX ");
			} else {
				System.out.print(x10 + " ");
			}
			x10 = x10 * 3;
			
		}
		System.out.println();System.out.println();
		
	}
	
	public static void ngulangKe4() {
		
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ( Kerjaan Ke-4 )");
		System.out.println();
		
		int n = 7;
		int soal1 = 1;
		int soal2 = 2;
		int soal3 = 1;
		int soal4 = 1;
		int soal5 = 1;
		int soal6 = 1;
		int soal7 = 2;
		int soal8 = 3;
		int soal9 = 4;
		int soal10 = 3;
		
		System.out.println("Soal 1");
		for (int i = 0; i < n; i++) {
			System.out.print(soal1 + " ");
			soal1 = soal1 + 2;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 2");
		for (int i = 0; i < n; i++) {
			System.out.print(soal2 + " ");
			soal2 = soal2 + 2;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 3");
		for (int i = 0; i < n; i++) {
			System.out.print(soal3 + " ");
			soal3 = soal3 + 3;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 4");
		for (int i = 0; i < n; i++) {
			System.out.print(soal4 + " ");
			soal4 = soal4 + 3;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 5");
		for (int i = 0; i < n; i++) {
			
			if (i == 2 || i == 5) {
				System.out.print("* ");
			} else {
				System.out.print(soal5 + " ");
				soal5 = soal5 + 4;
			}
			
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 6");
		for (int i = 0; i < n; i++) {
			
			if (i == 2 || i == 5) {
				System.out.print("* ");
			} else {
				System.out.print(soal6 + " ");
			}
			soal6 = soal6 + 4;
			
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 7");
		for (int i = 0; i < n; i++) {
			System.out.print(soal7 + " ");
			soal7 = soal7 * 2;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 8");
		for (int i = 0; i < n; i++) {
			System.out.print(soal8 + " ");
			soal8 = soal8 * 3;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 9");
		for (int i = 0; i < n; i++) {
			
			if (i == 2 || i == 5) {
				System.out.print("* ");
			} else {
				System.out.print(soal9 + " ");
				soal9  = soal9 * 4;
			}
			
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 10");
		for (int i = 0; i < n; i++) {
			
			if (i == 3) {
				System.out.print("XXX ");
			} else {
				System.out.print(soal10 + " ");
			}
			soal10 = soal10 * 3;
			
		}
		System.out.println();System.out.println();
		
	}
	
	public static void ngulangKe5() {
		
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ( Kerjaan Ke-5 )");
		System.out.println();
		
		int n = 7;
		int x1 = 1;
		int x2 = 2;
		int x3 = 1;
		int x4 = 1;
		int x5 = 1;
		int x6 = 1;
		int x7 = 2;
		int x8 = 3;
		int x9 = 4;
		int x10 = 3;
		
		System.out.println("Soal 1");
		for (int i = 0; i < n; i++) {
			System.out.print(x1 + " ");
			x1 = x1 + 2;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 2");
		for (int i = 0; i < n; i++) {
			System.out.print(x2 + " ");
			x2 = x2 + 2;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 3");
		for (int i = 0; i < n; i++) {
			System.out.print(x3 + " ");
			x3 = x3 + 3;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 4");
		for (int i = 0; i < n; i++) {
			System.out.print(x4 + " ");
			x4 = x4 + 3;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 5");
		for (int i = 0; i < n; i++) {
			
			if (i == 2 || i == 5) {
				System.out.print("* ");
			} else {
				System.out.print(x5 + " ");
				x5 = x5 + 4;
			}
			
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 6");
		for (int i = 0; i < n; i++) {
			
			if (i == 2 || i == 5) {
				System.out.print("* ");
			} else {
				System.out.print(x6 + " ");
			}
			x6 = x6 + 4;
			
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 7");
		for (int i = 0; i < n; i++) {
			System.out.print(x7 + " ");
			x7 = x7 * 2;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 8");
		for (int i = 0; i < n; i++) {
			System.out.print(x8 + " ");
			x8 = x8 * 3;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 9");
		for (int i = 0; i < n; i++) {
			
			if (i == 2 || i == 5) {
				System.out.print("* ");
			} else {
				System.out.print(x9 + " ");
				x9  = x9 * 4;
			}
			
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 10");
		for (int i = 0; i < n; i++) {
			
			if (i == 3) {
				System.out.print("XXX ");
			} else {
				System.out.print(x10 + " ");
			}
			x10 = x10 * 3;
			
		}
		System.out.println();System.out.println();
		
	}
	
	public static void ngulangKe6() {
		
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ( Kerjaan Ke-6 )");
		System.out.println();
		
		System.out.println("Soal 01");
		int a = 1;
		for (int i = 0; i < 7; i++) {
			System.out.print(a + " ");
			a = a + 2;
		}
		System.out.println();System.out.println();
		
		
		System.out.println("Soal 02");
		int b = 2;
		for (int i = 0; i < 7; i++) {
			System.out.print(b + " ");
			b = b + 2;
		}
		System.out.println();System.out.println();
		
		
		System.out.println("Soal 03");
		int c = 1;
		for (int i = 0; i < 7; i++) {
			System.out.print(c + " ");
			c = c + 3;
		}
		System.out.println();System.out.println();
		
		
		System.out.println("Soal 04");
		int d = 1;
		for (int i = 0; i < 7; i++) {
			System.out.print(d + " ");
			d = d + 3;
		}
		System.out.println();System.out.println();
		
		
		System.out.println("Soal 05");
		int e = 1;
		for (int i = 0; i < 7; i++) {
			
			if (i == 2 || i == 5) {
				System.out.print("*" + " ");
			} else if (i != 2 || i != 5) {
				System.out.print(e + " ");
				e = e + 4;
			}
			
		}
		System.out.println();System.out.println();
		
		
		System.out.println("Soal 06");
		int f = 1;
		for (int i = 0; i < 7; i++) {
			
			if (i == 2 || i == 5) {
				System.out.print("*" + " ");
			} else if (i != 2 || i != 5) {
				System.out.print(f + " ");
				
			}
			f = f +4;
		}
		System.out.println();System.out.println();
		
		
		System.out.println("Soal 07");
		int g = 2;
		for (int i = 0; i < 7; i++) {
			System.out.print(g + " ");
			g = g * 2;
		}
		System.out.println();System.out.println();
		
		
		System.out.println("Soal 08");
		int h = 3;
		for (int i = 0; i < 7; i++) {
			System.out.print(h + " ");
			h = h * 3;
		}
		System.out.println();System.out.println();
		
		
		System.out.println("Soal 09");
		int j = 4;
		for (int i = 0; i < 7; i++) {
			
			if (i == 2 || i == 5) {
				System.out.print("*" + " ");
				
				
			} else if (i != 2 || i != 5) {
				System.out.print(j + " ");
				j = j * 4;
			}
			
			
		}
		System.out.println();System.out.println();
		
		
		System.out.println("Soal 10");
		int k = 3;
		for (int i = 0; i < 7; i++) {
			
			if (i != 3) {
				System.out.print(k + " ");
			} else if (i == 3) {
				System.out.print("XXX" + " ");
			}
			k = k * 3;
			
		}
		System.out.println();System.out.println();
		
	}
	
	public static void ngulangKe7() {
		
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ( Kerjaan Ke-7 )");
		System.out.println();
		
		int n = 7;
		
		System.out.println("Soal 1");
		int x1 = 1;
		for (int i = 0; i < n; i++) {
			System.out.print(x1 + " ");
			x1 = x1 + 2;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 2");
		int x2 = 2;
		for (int i = 0; i < n; i++) {
			System.out.print(x2 + " ");
			x2 = x2 + 2;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 3");
		int x3 = 1;
		for (int i = 0; i < n; i++) {
			System.out.print(x3 + " ");
			x3 = x3 + 3;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 4");
		int x4 = 1;
		for (int i = 0; i < n; i++) {
			System.out.print(x4 + " ");
			x4 = x4 + 3;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 5");
		int x5 = 1;
		for (int i = 0; i < n; i++) {
			
			if (i == 2 || i == 5) {
				System.out.print("* ");
			} else {
				System.out.print(x5 + " ");
				x5 = x5 + 4;
			}
			
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 6");
		int x6 = 1;
		for (int i = 0; i < n; i++) {
			
			if (i == 2 || i == 5) {
				System.out.print("* ");
			} else {
				System.out.print(x6 + " ");
			}
			x6 = x6 + 4;
			
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 7");
		int x7 = 2;
		for (int i = 0; i < n; i++) {
			System.out.print(x7 + " ");
			x7 = x7 * 2;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 8");
		int x8 = 3;
		for (int i = 0; i < n; i++) {
			System.out.print(x8 + " ");
			x8 = x8 * 3;
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 9");
		int x9 = 4;
		for (int i = 0; i < n; i++) {
			
			if (i == 2 || i == 5) {
				System.out.print("* ");
			} else {
				System.out.print(x9 + " ");
				x9  = x9 * 4;
			}
			
		}
		System.out.println();System.out.println();
		
		System.out.println("Soal 10");
		int x10 = 3;
		for (int i = 0; i < n; i++) {
			
			if (i == 3) {
				System.out.print("XXX ");
			} else {
				System.out.print(x10 + " ");
			}
			x10 = x10 * 3;
			
		}
		System.out.println();System.out.println();
		
	}

}
