package day01;

import java.util.Scanner;

public class Looping_Condition {
	
	// membuat object
	static Scanner scn;
	
	public static void main(String[] args) {
		
		// instansiasi
		scn = new Scanner(System.in);
		
//		// tampilkan
//		System.out.println("Masukkan nama : ");
//		// variable
//		String nama = scn.nextLine();
//		
//		// tampilkan variable di layar
//		System.out.println("Nama : "+ nama);
		
//		int a = 3;
//		int b = 4;
//		int hasilTambah = tambah(a, b);
//		int hasilKurang = kurang(a, b);
//		int hasilKali = kali(a, b);
//		float hasilBagi = bagi(a, b);
//		
//		System.out.println("Hasil tambah = "+ hasilTambah);
//		System.out.println("Hasil kurang = "+ hasilKurang);
//		System.out.println("Hasil kali = "+ hasilKali);
//		System.out.println("Hasil bagi = "+ hasilBagi);
		
		System.out.println("Masukkan nilai n : ");
		int n = scn.nextInt();
		System.out.println();
		int a = 3;
		int b = 1;
		for (int i = 0; i < n; i++) {
			System.out.println(a);
			System.out.println(b);
			a = a+6;
			b = b+2;
		}
				
	}
	
	static int tambah(int x, int y) {
		return x+y;
	}
	
	static int kurang(int x, int y) {
		return x-y;
	}
	
	static int kali(int x, int y) {
		return x*y;
	}
	
	static float bagi(float x, float y) {
		return x/y;
	}

}
