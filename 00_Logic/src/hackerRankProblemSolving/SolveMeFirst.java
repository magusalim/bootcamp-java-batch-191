package hackerRankProblemSolving;

import java.util.*;

public class SolveMeFirst {
	
	static Scanner in;
	
	static int solveMeFirst(int a, int b) {
      // Hint: Type return a+b; below
      return a + b;
	}

	 public static void main(String[] args) {
        in = new Scanner(System.in);
        int a;
        System.out.print("Masukkan angka a : ");
        a = in.nextInt();
        int b;
        System.out.print("Masukkan angka b : ");
        b = in.nextInt();
        int sum;
        sum = solveMeFirst(a, b);
        System.out.println(sum);
	}
	
}
