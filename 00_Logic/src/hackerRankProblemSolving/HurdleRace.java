package hackerRankProblemSolving;

public class HurdleRace {

	public static void main(String[] args) {
		
		System.out.println(hurdleRace(4, new int[] {1, 6, 3, 5, 2}));
		System.out.println(hurdleRace(7, new int[] {2, 5, 4, 5, 2}));

	}
	
	public static int hurdleRace(int k, int[] height) {
		int max = height[0];
		
		for (int i = 1; i < height.length; i++) {
			if (height[i] > max) {
				max = height[i];
			}
		}
		if (k < max) {
			return max - k;
		} else {
			return 0;
		}
	}

}
