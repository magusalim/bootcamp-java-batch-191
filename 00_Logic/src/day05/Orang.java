package day05;

public class Orang {
	
	// property
	int id;
	String nama;
	String alamat;
	String gender;
	int umur;
	
	// constructor
	public Orang() {
		
	}
	
	public Orang(int id, String nama, String alamat, String gender, int umur) {
		this.id = id;
		this.nama = nama;
		this.alamat = alamat;
		this.gender = gender;
		this.umur = umur;
	}
	
	public Orang(int id, String nama, String alamat) {
		this.id = id;
		this.nama = nama;
		this.alamat = alamat;
	}
	
	public void showData() {
		System.out.println("ID \t: "+ this.id);
		System.out.println("Nama \t: "+ this.nama);
		System.out.println("Alamat \t: "+ this.alamat);
		System.out.println("Gender \t: "+ this.gender);
		System.out.println("Umur \t: "+ this.umur);
	}
}
