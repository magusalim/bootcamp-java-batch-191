package day05;

public class Main {
	
	public static void main(String[] args) {
		
		Orang org1 = new Orang(1, "Muhammad Agus", "Jakarta", "Pria", 20);
		System.out.println("Data Orang 1 :");
		org1.showData();
		
		Orang org2 = new Orang(1, "Reno", "Depok");
		System.out.println("Data Orang 2 :");
		org2.showData();
		
		Orang org3 = new Orang();
		System.out.println("Data Orang 3 :");
		org3.showData();
		
		Orang org4 = org1;
		System.out.println("Data Orang 4 :");
		org4.nama = "Asdf";
		org4.showData();
		
		System.out.println("Data Orang 1 :");
		org1.showData();
		
		System.out.println();
		System.out.println();
		
		// Motor
		System.out.println("========== MOTOR ==========");
		
		Motor mtr1 = new Motor("Yamaha", "Biru", 2);
		System.out.println();
		mtr1.showMotor();
		
		Motor mtr2 = new Motor("Honda", "Merah");
		System.out.println();
		mtr2.showMotor();
		
		Motor mtr3 = new Motor("Suzuki", 2);
		System.out.println();
		mtr3.showMotor();
		
		System.out.println();
		System.out.println();
		
		// Mobil
		System.out.println("========== MOBIL ==========");
		
		Mobil mbl1 = new Mobil("BMW", "Hitam", "Pertamax Turbo", 4);
		System.out.println();
		mbl1.showMobil();
		
		Mobil mbl2 = new Mobil("Mercedes Benz", 2);
		System.out.println();
		mbl2.showMobil();
		
		Mobil mbl3 = new Mobil("Isuzu", "Biru");
		System.out.println();
		mbl3.showMobil();
		
		System.out.println();
		System.out.println();
		
		// Helm
		System.out.println("========== HELM ==========");
		
		Helm helm1 = new Helm("KYT");
		helm1.warna = "Biru";
		System.out.println();
		helm1.showHelm();
		
		Helm helm2 = new Helm("NHK");
		System.out.println();
		helm2.showHelm();

		System.out.println();
		System.out.println();
		
		// Kursi
		System.out.println("========== KURSI ==========");
		
		Kursi krs1 = new Kursi("Abu-Abu", "Empuk");
		System.out.println();
		krs1.showKursi();
		
		Kursi krs2 = new Kursi("Putih");
		System.out.println();
		krs2.showKursi();

		System.out.println();
		System.out.println();
		
	}
	
}
