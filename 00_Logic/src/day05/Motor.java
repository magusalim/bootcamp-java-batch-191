package day05;

public class Motor {
	
	String merk;
	String warna;
	int spion;	
	
	public Motor() {
		
	}
	
	public Motor(String merk, String warna, int spion) {
		this.merk = merk;
		this.warna = warna;
		this.spion = spion;
	}
	
	public Motor(String merk, String warna) {
		this.merk = merk;
		this.warna = warna;
	}
	
	public Motor(String merk, int spion) {
		this.merk = merk;
		this.spion = spion;
	}
	
	public void showMotor() {
		System.out.println("Merk \t: "+ this.merk);
		System.out.println("Warna \t: "+ this.warna);
		System.out.println("Spion \t: "+ this.spion);
	}

}
