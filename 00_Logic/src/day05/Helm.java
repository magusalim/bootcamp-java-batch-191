package day05;

public class Helm {

	String merk;
	String warna;
	
	public Helm() {
		
	}
	
	public Helm(String merk, String warna) {
		this.merk = merk;
		this.warna = warna;
	}
	
	public Helm(String merk) {
		this.merk = merk;
	}
	
//	public Helm(String warna) {
//		this.warna = warna;
//	}
	
	public void showHelm() {
		System.out.println("Merk \t: "+ this.merk);
		System.out.println("Warna \t: "+ this.warna);
	}
	
}
