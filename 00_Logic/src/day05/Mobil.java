package day05;

public class Mobil {
	
	String merk;
	String warna;
	String bahanBakar;
	int tempatDuduk;
	
	public Mobil() {
		
	}
	
	public Mobil(String merk, String warna,	String bahanBakar, int tempatDuduk) {
		this.merk = merk;
		this.warna = warna;
		this.bahanBakar = bahanBakar;
		this.tempatDuduk = tempatDuduk;
	}
	
	public Mobil(String merk, int tempatDuduk) {
		this.merk = merk;
		this.tempatDuduk = tempatDuduk;
	}
	
	public Mobil(String merk, String warna) {
		this.merk = merk;
		this.warna = warna;
	}
	
	public void showMobil() {
		System.out.println("Merk \t\t\t: "+ this.merk);
		System.out.println("Warna \t\t\t: "+ this.warna);
		System.out.println("Bahan bakar \t\t: "+ this.bahanBakar);
		System.out.println("Banyaknya tempat duduk \t: "+ this.tempatDuduk);
	}

}
