package day05;

public class Kursi {
	
	String warna;
	String bahan;
	
	public Kursi() {
		
	}
	
	public Kursi(String warna, String bahan) {
		this.warna = warna;
		this.bahan = bahan;
	}
	
	public Kursi(String warna) {
		this.warna = warna;
	}
	
	public void showKursi() {
		System.out.println("Warna \t: "+ this.warna);
		System.out.println("Bahan \t: "+ this.bahan);
	}
	
}
