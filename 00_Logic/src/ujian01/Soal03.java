package ujian01;

import java.util.Scanner;

public class Soal03 {

	static Scanner in;
	
	public static void main(String[] args) {
		
        in = new Scanner(System.in);
        
        System.out.println("Masukkan banyaknya angka : ");
        int n = in.nextInt();
        System.out.println("Masukkan banyaknya rotasi : ");
        int rotation = in.nextInt();
        
        int[] arr = new int[n];

        for(int i = 0; i < n; i++) {
            if(rotation > i) 
                arr[n - rotation + i] = in.nextInt();
            else
                arr[i - rotation] = in.nextInt();
        }
        
        for(int i = 0; i < n; i++)
            System.out.print(arr[i] + " ");
    }
	
}

