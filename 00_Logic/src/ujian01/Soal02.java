package ujian01;

import java.util.Scanner;

public class Soal02 {
	
	static Scanner in;
	
	public static void main(String args[]) {
		
	    String word;
	    
	    in = new Scanner(System.in);
	 
	    System.out.println("Masukkan kata : ");
	    word = in.nextLine();
	 
	    int length  = word.length();
	    
	    int i;
	   	 
	    int awal  = 0;
	    int akhir = length - 1;
	    int middle = (awal + akhir)/2;
	 
	    for (i = awal; i <= middle; i++) {
	    	if (word.toLowerCase().charAt(awal) == word.toLowerCase().charAt(akhir)) {
	    		awal++;
	    		akhir--;
	    	} else {
	    		break;
	    	}
	    }
	    
	    System.out.println();
	    if (i == middle + 1) {
	    	System.out.println("YES");
	    } else {
	    	System.out.println("NO");
	    }  
    }
}
