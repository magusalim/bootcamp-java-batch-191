package ujian01;

import java.util.Scanner;

public class Soal09 {

	static Scanner in;
	
	public static void main(String[] args) {
		
		in = new Scanner(System.in);
		
		System.out.print("Masukkan jarum jam \t: ");
		int jam = in.nextInt();
		System.out.print("Masukkan jarum menit \t: ");
		int menit = in.nextInt();
		
		System.out.println(clockAngle(jam, menit)+" derajat");
		
	}
	 
    public static int clockAngle(double h, double m) 
    { 
        // validasi masukkan
        if (h <0 || m < 0 || h >12 || m > 60) {
            System.out.println("Salah masukkan!");
        }
        if (h == 12) {
        	h = 0;
        }
        if (m == 60) {  
            m = 0;
        }
  
        // Hitung sudut yang digerakkan oleh jarum jam dan menit dengan mengacu pada 12:00
        int hour_angle = (int)(0.5 * (h*60 + m)); 
        int minute_angle = (int)(6*m); 
  
        // Temukan perbedaan antara dua sudut
        int angle = Math.abs(hour_angle - minute_angle); 
  
        // Sudut yang lebih kecil dari kemungkinan dua sudut
        angle = Math.min(360-angle, angle); 
  
        return angle; 
    }
}
