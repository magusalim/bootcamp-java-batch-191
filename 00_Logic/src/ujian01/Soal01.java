package ujian01;

import java.util.Arrays;
import java.util.Scanner;

public class Soal01 {
	
	static Scanner scan;
	
	public static int getMoneySpent(int[] kacamata, int[] baju, int budget) {
        
		Arrays.sort(kacamata);
        Arrays.sort(baju);
        
        int max = -1;
        int i = 0;
        int j = baju.length - 1;
        
        while (i < kacamata.length && j >= 0) {
            int cost = kacamata[i] + baju[j];
            if (cost > budget) {
                j--; // look for a cheaper hard drive
            } else {
                if (cost > max) {
                    max = cost;
                }
                i++; // look for a more expensive keyboard
            }
        }
        
        return max;
    }

    public static void main(String[] args) {
    	
        scan = new Scanner(System.in);
        
        System.out.println("Masukkan budget : ");
        int budget = scan.nextInt();
        System.out.println("Masukkan banyaknya kacamata : ");
        int kacamata = scan.nextInt();
        System.out.println("Masukkan banyaknya baju : ");
        int baju = scan.nextInt();
        
        int[] kcmt = new int[kacamata];
        
        for (int i = 0; i < kacamata; i++) {
        	System.out.print("Masukkan harga kacamata ke-"+ (i+1) +" = ");
            kcmt[i] = scan.nextInt();
        }
        
        System.out.println();
        
        int[] bju = new int[baju];
        
        for (int i = 0; i < baju; i++) {
        	System.out.print("Masukkan harga baju ke-"+ (i+1) +" = ");
            bju[i] = scan.nextInt();
        }
        
//        scan.close();
        
        System.out.println();
        int total = getMoneySpent(kcmt, bju, budget);
        System.out.println("Total uang Andi yang dikeluarkan = "+ total);
    }
}
