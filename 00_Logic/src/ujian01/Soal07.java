package ujian01;

import java.util.Scanner;

public class Soal07 {

	static Scanner in;
	
	public static void main(String[] args) {
        
		in = new Scanner(System.in);

        System.out.print("Masukkan banyaknya nilai : ");
        int n = in.nextInt();

        System.out.println("(1) Cari nilai MEAN");
        System.out.println("(2) Cari nilai MEDIAN");
        System.out.println("(3) Cari nilai MODUS");
        
        System.out.print("Pilih antara (1) -- (3) : ");
        int option = in.nextInt();
        
        switch(option) {

        case 1:
        	
        	int[] za = new int[n];
            int jumlah = 0;

            for(int i = 0; i < za.length; i++) {
                System.out.print("Masukkan nilai ke-"+ (i+1) +" = ");
                za[i]=in.nextInt();
            }
            
            System.out.println();
            System.out.print("Tampilan semua nilai : ");
            for(int k = 0; k < za.length; k++){
            	System.out.print(za[k] +" ");
            	jumlah += za[k];
            }

            int mean = jumlah / n;
            System.out.println();
            System.out.println("Nilai MEAN = "+ mean);
            
            break;

        case 2:
        	
        	int[]nilai = new int[n];
        	
        	System.out.println();
        	for(int i = 0; i < nilai.length; i++) {
        		System.out.print("Masukan nilai ke-"+ (i+1) +" = ");
        		nilai[i] = in.nextInt();
            }
        	
            float median = 0.0f;
            int bil = nilai.length%2;
            if(bil == 0) {
                int posisi = n / 2;
                
                median = (nilai[posisi-1] + nilai[posisi]) / 2;
                System.out.println();
                System.out.printf("Nilai MEDIAN = %.2f", median);
            } else {
                int posisi = n / 2;
                
                median = nilai[posisi];
                System.out.println();
                System.out.print("Nilai MEDIAN = "+ median);
            }
            
            break;
        
        case 3:
        	
        	System.out.println();
        	
        	int[]a = new int[n];
            int c = a[0];
            int b = 0;
            int hasil = 0;
            int besar = 0;
            
            for(int i = 0; i < a.length; i++) {
            	System.out.print("Masukkan nilai ke-"+ (i+1) +" = ");
            	a[i] = in.nextInt();
            }
           
            System.out.println();
        	for (int i = 0; i<a.length ;i++){
        		if(a[i] == c) {
        			System.out.print(a[i] +" ");
        			System.out.println(++b);
        			if(b > besar){
        				besar = b;
        				hasil = c;
        			}
        		} else {
        			c = a[i];
        			b = 1;
        			System.out.print(a[i] +" ");
        			System.out.println(b);
        		}
        	}
        	
            System.out.println();	
        	System.out.println("Modus = "+ hasil);
            
        	break;    

        default:
        	
        	System.out.println("Pilihan salah!");
        	
        	break;
    }
  }
}