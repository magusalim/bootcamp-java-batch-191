package hackerRankJava;

import java.util.Scanner;

public class JavaOutputFormatting {
	
	static Scanner sc, scn01;
	
	public static void main(String[] args) {
		
        sc = new Scanner(System.in);
        
        System.out.println("================================");
        
        for(int i = 0; i < 3; i++){
        	
        	//Melakukan Input String 
            String s1 = sc.next();
            
            //Melakukan Input Integer
            int x = sc.nextInt();
        	
            
            /*printf untuk format 
             %-14s 	: Justify rata kiri dengan banyak karakter 14
             %03d 	: Integer dengan format angka 0 sebanyak 3 digit
             %n 	: Baris baru */
            System.out.printf("%-14s %03d %n", s1, x);
        }
        System.out.println("================================");
        
        ulang1();
       
	}
	
	public static void ulang1() {
		scn01 = new Scanner(System.in);
		
		for(int j = 0; j < 3; j++) {
			String s1 = scn01.next();
			int i1 = scn01.nextInt();
			
			System.out.printf("%-14s %03d %n", s1, i1);
		}
	}
	
}
