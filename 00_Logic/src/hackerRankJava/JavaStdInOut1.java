package hackerRankJava;

import java.util.Scanner;

public class JavaStdInOut1 {

	static Scanner scan;
	
	public static void main(String[] args) {
        scan = new Scanner(System.in);
        System.out.println("Masukkan nilai a : ");
        int a = scan.nextInt();
        System.out.println("Masukkan nilai b : ");
        int b = scan.nextInt();
        System.out.println("Masukkan nilai c : ");
        int c = scan.nextInt();

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }

}
