package hackerRankJava;

import java.util.Scanner;

public class JavaLoops1 {
	
	static Scanner scn = new Scanner(System.in);
	static Scanner scn01;
	
    public static void main(String[] args) {
    	System.out.print("Masukkan Integer : ");
        int N = scn.nextInt();
        int hasil = 0;
        for(int i = 1; i <= 10; i++){
            hasil = N * i;
            System.out.println(N + " x " + i + " = " + hasil);
        }
        
        //Panggil Method
        ulang1();

    }
    
    public static void ulang1() {
    	//Membuat input
    	scn01 = new Scanner(System.in);
    	int hasil1 = 0;
    	
    	System.out.print("Masukkan Integer 1 : ");
    	int n1 = scn01.nextInt();
    	
    	//Perulangan
    	for(int i = 1; i <= 10; i++) {
    		hasil1 = n1 * i;
    		System.out.println(n1 + " x " + i + " = " + hasil1);
    	}
    }
    
}
