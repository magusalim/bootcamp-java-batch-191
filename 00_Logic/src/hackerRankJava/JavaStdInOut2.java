package hackerRankJava;

import java.util.Scanner;

public class JavaStdInOut2 {

	static Scanner scan;
	
	public static void main(String[] args) {
        scan = new Scanner(System.in);
        System.out.println("Masukkan nilai Integer : ");
        int i = scan.nextInt();
        System.out.println("Masukkan nilai Double : ");
        double d = scan.nextDouble();
        System.out.println("Masukkan nilai String : ");
        String s = scan.next();

        System.out.println("String: " + s);
        System.out.println("Double: " + d);
        System.out.println("Int: " + i);
    }
	
}
