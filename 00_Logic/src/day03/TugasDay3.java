package day03;

import java.util.Scanner;

import common.PrintArray;

public class TugasDay3 {

	static Scanner input;
	
	public static void main(String[] args) {
		
//		soalKe01();
		soalKe02();

	}
	
	public static void soalKe01() {
		
		input = new Scanner(System.in);
		
		// Inputan
		System.out.print("Masukkan nilai N : ");
		int n = input.nextInt();
		System.out.print("Masukkan nilai M : ");
		int m = input.nextInt();
		System.out.print("Masukkan nilai O : ");
		int o = input.nextInt();
		
		System.out.println();
		
		// Logic
		
		// 1. buat deret array
		int[] deret = new int[n * 4];
		int angka = o;
		for (int i = 0; i < deret.length; i++) {
			
			if (i % 4 == 3) {
				deret[i] = m;
			} else {
				deret[i] = angka;
				angka = angka + m;
			}
			System.out.print("[ "+ deret[i] +" ]");
			
		}
		System.out.println();System.out.println();
		
		// 2. buat array 2 dimensi
		String[][] array = new String[n][n];
		
		// 3. buat variabel index
		int index = 0;
		
		// 4. isi baris atas
		for (int i = 0; i < n; i++) {
			array[0][i] = deret[index]+"";
			index++;
		}
		
		// 5. isi baris kanan
		for (int i = 1; i < n; i++) {
			array[i][n-1] = deret[index]+"";
			index++;
		}
		
		// 6. isi baris bawah
		for (int i = n-2; i >= 0; i--) {
			array[n-1][i] = deret[index]+"";
			index++;
		}
		
		// 7. isi baris kiri
		for (int i = n-2; i > 0 ; i--) {
			array[i][0] = deret[index]+"";
			index++;
		}
		
		// menampilkan dari class PrintArray package common
		PrintArray.array2D(array);
		
	}
	
	public static void soalKe02() {
		
		input = new Scanner(System.in);
		

		// Inputan
		System.out.print("Masukkan nilai N : ");
		int n = input.nextInt();
		System.out.print("Masukkan nilai M : ");
		int m = input.nextInt();
		System.out.print("Masukkan nilai O : ");
		int o = input.nextInt();
		
		System.out.println();
		
		// Logic
		
		// buat deret array
		int[] deret = new int[n * 3];
		int angka = o;
		for (int i = 0; i < deret.length; i++) {
			
			if (i % 4 == 3) {
				deret[i] = m;
				
			} else {
				deret[i] = angka;
				angka = angka + m;
			}
			System.out.print("[ "+ deret[i] +" ]");
			
		}
		System.out.println();System.out.println();
		
		// buat array 2 dimensi
		String[][] array = new String[n][n];
		
		// buat variabel index
		int index = 0;
		
		// isi baris diagonal
		for (int i = 0; i < n; i++) {
			array[i][n-1-i] = "*";
			index++;
		}
		
		// isi baris kanan
//		for (int i = 1; i < n; i++) {
//			
//		}
//		
//		// isi baris bawah
//		for (int i = n-2; i > 0 ; i--) {
//			
//		}
		
		// menampilkan
		PrintArray.array2D(array);
		
	}

}
