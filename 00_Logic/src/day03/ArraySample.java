package day03;

import java.util.Scanner;

public class ArraySample {

	static Scanner input;
	
	public static void main(String[] args) {
		
//		cobaKe01();
//		cobaKe02();
		cobaKe03();
		
	}
	
	public static void cobaKe01() {
		
		// Soal
		// 3 1 9 3 15 5
		
		input = new Scanner(System.in);
		
		System.out.println("Masukkan nilai n : ");
		int n = input.nextInt();
		
		int a = 3;
		int b = 1;
		int[] array = new int[n];
		
		for (int i = 0; i < array.length; i++) {
			if (i % 2 == 0) {
				array[i]=a;	
				a = a + 6;
			} else {
				array[i]=b;
				b = b + 2;
			}
			System.out.print(array[i] + "\t");
		}
		
	}
	
	public static void cobaKe02() {
		
		// Soal
		// 3 1 9 3 15 5
		
		input = new Scanner(System.in);
		
		// Input nilai n
		System.out.print("Masukkan nilai n :");
		int n = input.nextInt();
		
		// Logic
		
		int angka1 = 3;		// deklarasi variabel
		int angka2 = 1;
		
		int[] array = new int[n];	// deklarasi object array berdasarkan inputan
		
		for (int i = 0; i < array.length; i++) {
			if (i % 2 == 0) {
				array[i] = angka1;		// deklarasi
				angka1 += 6;
			} else {
				array[i] = angka2;
				angka2 += 2;
			}
			System.out.print(array[i]+"\t");
		}
		
	}
	
	public static void cobaKe03() {
		
		input = new Scanner(System.in);
		
		// Input nilai n
		System.out.print("Masukkan n : ");
		int n = input.nextInt();
		
		// Logic
		
		int a1 = 3;
		int a2 = 1;
		
		int[] array = new int[n];
		
		for (int i = 0; i < array.length; i++) {
			if (i % 2 == 0) {
				array[i] = a1;
				a1 +=6;
			} else {
				array[i] = a2;
				a2 +=2;
			}
			System.out.print(array[i] +"\t");
		}
		
	}

}
