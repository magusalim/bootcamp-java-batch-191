package day03;

public class Array2d {

	public static void main(String[] args) {
		
		int[][] array1 = new int[5][3];
		
		int angka = 1;

		// mengisi 
		for (int i = 0; i < array1.length; i++) {
			for (int j = 0; j < array1[i].length; j++) {
				array1[i][j]=angka;
				angka += 3;
			}
		}
		
		// menampilkan
		for (int a= 0; a < array1.length; a++) {
			for (int b = 0; b < array1[a].length; b++) {
				System.out.print(array1[a][b] + "\t");
			}
			
			// pindah baris
			System.out.println("\n");
		}

	}

}
