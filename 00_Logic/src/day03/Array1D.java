package day03;

public class Array1D {

	public static void main(String[] args) {
		
		// deklarasi array
		int[] array1 = new int[10];
		
		// isi value element array
		array1[0]=1;
		array1[1]=2;
		array1[2]=3;
		array1[3]=4;
		array1[4]=5;
		array1[5]=6;
		array1[6]=7;
		array1[7]=8;
		array1[8]=9;
		array1[9]=10;
		
		// cek error
//		array1[10]=1;
		
		System.out.print(array1.length);
		System.out.println();
		for (int i = 0; i < array1.length; i++) {
			System.out.print(array1[i]);
			
		}
		System.out.println();
		
		int[] array2 = new int[] {1,2,3,4,5};
		System.out.print(array2.length);
		System.out.println();
		for (int i = 0; i < array2.length; i++) {
			System.out.print(array2[i]);
		}
		System.out.println();

	}

}
