package day02;

import java.util.Scanner;

public class TugasDay2 {
	
	static Scanner input;

	public static void main(String[] args) {
		
		System.out.println("Tugas Day 2");
		System.out.println();
		
		// Soal 1
		soalKe01();
		System.out.println();System.out.println();

		// Soal 2
		soalKe02();
		System.out.println();System.out.println();
		
		// Soal 3
		soalKe03();
		System.out.println();System.out.println();
		
		// Soal 4
		soalKe04();
		System.out.println();System.out.println();
		
		//Soal 5
		soalKe05();
		System.out.println();System.out.println();
		
		// Soal 6
		soalKe06();
		System.out.println();System.out.println();

	}
	
	public static void soalKe01() {
		
		input = new Scanner(System.in);
		
		System.out.println("[ Soal 01 ]");
		
		// Perulangannya
		System.out.print("Perulangannya sebanyak : ");
		int n1 = input.nextInt();
		
		// Algoritmanya
		System.out.print("Algoritma : ");
		int n2 = input.nextInt();
		
		System.out.println("---------------------------");
		
		int N = 1;
		
		int d0 = N;
		int d1 = N;
		int d2 = N;
		
		// Deret
		System.out.print("Deret : ");
		for (int i = 0; i < n1; i++) {
			System.out.print("[ "+ i +" ]");
		}
		System.out.println();
		
		// N
		System.out.print("N ("+ n1 +") : ");
		for (int i = 0; i < n1; i++) {
			
			if (i == 0) {
				d0 = N;
			} else if (i == 1) {
				d1 = N;
			} else if (i == 2) {
				d2 = N;
			}
			
			System.out.print("[ " + N + " ]");
			N = N * n2;
			
			
		}
		System.out.println();System.out.println();
		
		// Perhitungan
		System.out.print("Perhitungan => ");
		
		int hasil = d0 + d1 + d2;
		System.out.print(d0 + " + " + d1 + " + " + d2 + " = " + hasil);
		
	}
	
	public static void soalKe02() {
		
		input = new Scanner(System.in);
		
		// Soal 02
		System.out.println("[ Soal 02 ]");
		
		// Perulangannya
		System.out.print("Perulangannya sebanyak : ");
		int n1 = input.nextInt();
		
		// Algoritmanya
		System.out.print("Algoritma ke-1 : ");
		int n2 = input.nextInt();
		
		System.out.println("---------------------");
		
		int N = 1;
		int a = -1;
		
		int d0 = N;
		int d1 = N;
		int d2 = N;
		
		// Deret
		System.out.print("Deret : ");
		for (int i = 0; i < n1; i++) {
			System.out.print("[ "+ i +" ]");
		}
		System.out.println();
		
		// N
		System.out.print("N ("+ n1 +") : ");
		for (int i = 0; i < n1; i++) {
			
			if (i == 2 || i == 5) {
				N = N * a;
				System.out.print("[ "+ N +" ]");
			} else {
				System.out.print("[ " + N + " ]");
				
			}
			
			if (i == 0) {
				d0 = N;
			} else if (i == 1) {
				d1 = N;
			} else if (i == 2) {
				d2 = N;
			}
			
			N = N * n2;
			
		}
		System.out.println();System.out.println();
		
		// Perhitungan
		System.out.print("Perhitungan => ");
		
		int hasil = d0 + d1 + d2;
		System.out.print(d0 + " + " + d1 + " + " + d2 + " = " + hasil);
		
	}
	
	public static void soalKe03() {
		
		input = new Scanner(System.in);
		
		// Soal 03
//		System.out.println("[ Soal 03 ]");
//		
//		System.out.print("Input nilai N1 : ");
//		int n1 = input.nextInt();
//		System.out.print("Input nilai N2 : ");
//		int n2 = input.nextInt();
//		System.out.print("Input nilai N3 : ");
//		int n3 = input.nextInt();
////		
//		System.out.println("---------------------");
//		
//		int xN1 = 2;
//		int xN2 = 2;
//		int xN3 = 2;
//		
////		int i0 = xN1;
////		int i1 = xN1;
////		int i2 = xN1;
//		
//		// Deret
//		System.out.print("D  : ");
//		for (int i = 0; i < n1; i++) {
//			System.out.print("[ "+ i +" ]");
//		}
//		System.out.println();
//		
//		// N1
//		System.out.print("N1 : ");
//		for (int i = 0; i < n1; i++) {
//			
//			if (i == 0) {
//				System.out.print("[ " + xN1 + " ]");
//				xN1 = xN1 * 3;
//			} else if (i == 5) {
//				System.out.print("[ " + xN1 + " ]");
//				xN1 = xN1 / 3;
//			} else if (i >= 3) {
//				System.out.print("[ " + xN1 + " ]");
//				xN1 = xN1 / 2;
//			} else  {
//				System.out.print("[ " + xN1 + " ]");
//				xN1 = xN1 * 2;
//			} 
//			
//		}
//		System.out.println();
//		
//		// N2
//		System.out.print("N2 : ");
//		for (int i = 0; i < n2; i++) {
//			
//			if (i == 0) {
//				System.out.print("[ " + xN2 + " ]");
//				xN2 = xN2 * 3;
//			} else if (i == 5) {
//				System.out.print("[ " + xN2 + " ]");
//				xN2 = xN2 / 3;
//			} else if (i >= 3) {
//				System.out.print("[ " + xN2 + " ]");
//				xN2 = xN2 / 2;
//			} else  {
//				System.out.print("[ " + xN2 + " ]");
//				xN2 = xN2 * 2;
//			} 
//			
//		}
//		System.out.println();
//		
//		// N3
//		System.out.print("N3 : ");
//		for (int i = 0; i < n3; i++) {
//			
//			if (i == 0) {
//				System.out.print("[ " + xN3 + " ]");
//				xN3 = xN3 * 3;
//			} else if (i == 5) {
//				System.out.print("[ " + xN3 + " ]");
//				xN3 = xN3 / 3;
//			} else if (i >= 3) {
//				System.out.print("[ " + xN3 + " ]");
//				xN3 = xN3 / 2;
//			} else  {
//				System.out.print("[ " + xN3 + " ]");
//				xN3 = xN3 * 2;
//			} 
//			
//		}
//		System.out.println();
		
	}
	
	public static void soalKe04() {
		
		input = new Scanner(System.in);
		
		// Soal 04
//		System.out.println("[ Soal 04 ]");
//		
//		System.out.print("Input nilai N1 : ");
//		int n1 = input.nextInt();
////		System.out.print("Input nilai N2 : ");
////		int n2 = input.nextInt();
//				
//		System.out.println("---------------------");
//		
//		int xN1 = 1;
//		int xN11 = 5;
//		
//		int xN2 = 1;
//		int xN3 = 1;
//		
////				int i0 = xN1;
////				int i1 = xN1;
////				int i2 = xN1;
//		
//		// Deret
//		System.out.print("D  : ");
//		for (int i = 0; i < n1; i++) {
//			System.out.print("[ "+ i +" ]");
//		}
//		System.out.println();
//		
//		// N1
//		System.out.print("N1 : ");
//		for (int i = 0; i < n1; i++) {
//			
//			System.out.print("[ "+ xN1 +" ]");
//			xN1 = xN1 + 1;
//			
//			if (i % 2 == 0) {
//				System.out.print("[ "+ xN11 +" ]");
//				xN11 = xN11 + 5;
//			}
//			
////			if (i == i + 0) {
////				System.out.print("[ "+ xN1 +" ]");
////				xN1 = xN1 + 1;
////			} else if (i % 2 == 0) {
////				System.out.print("[ "+ xN11 +" ]");
////				xN11 = xN11 + 5;
////			}
//			
////			for (int j = 0; j < n1; j++) {
////
////				System.out.print("[ "+ xN1 +" ]");
////				xN1 = xN1 +1;
////				
////			}
////			
////			System.out.print("[ "+ xN11 +" ]");
////			xN11 = xN11 + 5;
//			
//		}
//		System.out.println();
		
		// N2
//		System.out.print("N2 : ");
//		for (int i = 0; i < n2; i++) {
//			
//			if (i == 0) {
//				System.out.print("[ " + xN2 + " ]");
//				xN2 = xN2 * 3;
//			} else if (i == 5) {
//				System.out.print("[ " + xN2 + " ]");
//				xN2 = xN2 / 3;
//			} else if (i >= 3) {
//				System.out.print("[ " + xN2 + " ]");
//				xN2 = xN2 / 2;
//			} else  {
//				System.out.print("[ " + xN2 + " ]");
//				xN2 = xN2 * 2;
//			} 
//			
//		}
//		System.out.println();
		
	}
	
	public static void soalKe05() {
		
		input = new Scanner(System.in);
		
		// Soal 05
		System.out.println("[ Soal 05 ]");
		
		// Input Teks
		System.out.println("Masukkan teks : ");
		String text = input.nextLine();
		
		// Logic
		
		// split text to word
		String[] array = text.split(" ");
		
		// tampilin banyak word
		for (int i = 0; i < array.length; i++) {
			
			// split word to character/item
			String[] item = array[i].split("");
			
			// tampilin banyak character/item
			for (int j = 0; j < item.length; j++) {
				if (j > 0 && j < item.length - 1) {
					
					// cetak "*"
					System.out.print("*");
				} else {
					
					// cetak character
					System.out.print(item[j]);
				}
			}
			
			// kasih spasi tiap word
			System.out.print(" ");
		}
		
	}
	
	public static void soalKe06() {
		
		input = new Scanner(System.in);
		
		// Soal 06
		System.out.println("[ Soal 06 ]");
		
		// Input Teks
		System.out.print("Masukkan teks : ");
		String text = input.nextLine();
		
		System.out.println();
		
		// Logic
		
		// tempat simpan kata nya
		int sum = 0;
		
		// perulangan banyak kata dari kalimat inputan
		for (int i = 0; i < text.length(); i++) {
			
			// kondisi kalo karakter ada uppercase dari kalimat inputan
			if (Character.isUpperCase(text.charAt(i))) {
				
				// jumlah banyak kata nya
				sum++;
			}
		}
		// cetak output
		System.out.println("Output => " + sum + " kata");
		
	}

}
