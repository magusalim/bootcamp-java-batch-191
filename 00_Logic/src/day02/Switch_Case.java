package day02;

import java.util.Scanner;

public class Switch_Case {
	
	static Scanner input;

	public static void main(String[] args) {
		
		cobaSwitchCase();
		

	}
	
	public static void cobaSwitchCase() {
				
		input = new Scanner(System.in);
				
		System.out.print("Masukkan score Anda ( A / B / C / D / E ) : ");
		String grade = input.next();
		System.out.println();
		
		
		
		switch (grade) {
		case "A" :
		case "B" :
			System.out.println("Selamat Anda lulus tanpa revisi!");
			break;
		case "C" :
			System.out.println("Selamat Anda lulus, dengan revisi!");
			break;
		case "D" :
		case "E" :
			System.out.println("Mohon maaf Anda tidak lulus");
			break;

		default:
			System.out.println("Invalid grade input!");
			break;
		}
		
	}

}
