package day07;

public class AbstractClass01 {

	public static void main(String[] args) {
		
		// Bike
		Bike01 obj1 = new Honda01();
		Bike01 obj2 = new Yamaha01();
		Bike01 obj3 = new Vespa01();
		
		obj1.run();
		obj2.run();
		obj3.run();
		
		// Car
		Car01 car1 = new BMW01();
		Car01 car2 = new Toyota01();
		Car01 car3 = new Panther01();
		
		System.out.println();
		car1.run();
		System.out.println("Kecepatannya : "+ car1.speed());
		
		System.out.println();
		car2.run();
		System.out.println("Kecepatannya : "+ car2.speed());
		
		System.out.println();
		car3.run();
		System.out.println("Kecepatannya : "+ car3.speed());
		
		// Laptop
		Laptop01 lap1 = new Asus01();
		Laptop01 lap2 = new HP01();
		Laptop01 lap3 = new Acer01();
		
		lap1.merk();
		lap2.color();
		lap3.spec();

	}

}
