package com.xsis.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Biodata;
import com.xsis.demo.model.Employee;
import com.xsis.demo.repository.EmployeeRepo;

@Controller
public class EmployeeController {

	//	membuat auto instance dari repository
	@Autowired
	private EmployeeRepo repo;
	
	
	//	request halaman index yang ada di url localhost:port/employee
	//	dengan membawa data dari model
	@RequestMapping("/employee")
	public String index(Model model) {
		
		//	membuat object list employee,
		//	kemudian diisi dari object repo dengan method findAll
		List<Employee> data = repo.findAll();
		
		//	mengirim variable listData, value diisi dari object data
		model.addAttribute("listData", data);
		
		return "/employee/index";
	}
	
	
	//	request halaman add yang ada di url localhost:port/employee/add
	@RequestMapping(value="/employee/add")
	public String add() {
		
		return "/employee/add";
	}
	
	
	//	melakukan save data ke dalam model Employee
	//	dilengkapi dengan method post
	@RequestMapping(value="/employee/save", method=RequestMethod.POST)
	public String save(@ModelAttribute Employee item) {
		
		//	mengirim item agar dapat disave ke dalam database 
		repo.save(item);
		
		return "redirect:/employee";
	}
	
	
	//	request edit data berdasarkan id spesifik
	@RequestMapping(value="/employee/edit/{id}")
	public String edit(Model model, @PathVariable(name="id") Long id) {
		
		//	mencari data yang mau di edit dalam Employee dengan id spesifik
		Employee item = repo.findById(id).orElse(null);
		
		//	mengirim variable data, value diisi dari object item
		model.addAttribute("data", item);
		
		return "employee/edit";
	}
	
	//	request halaman delete yang ada di url localhost:port/employee/delete
	//	dengan berdasarkan id spesifik
	//	dengan menggunakan method get
	@RequestMapping(value="/employee/delete/{id}", method=RequestMethod.GET)
	public String delete(Model model, @PathVariable(name="id") Long id) {
		
		//	mencari data yang mau di delete dalam Employee dengan id spesifik
		Employee item = repo.findById(id).orElse(null);
		
		//	mengirim variable data, value diisi dari object item
		model.addAttribute("data", item);
			
		return "employee/delete";
	}
	
	//	request delete data
	@RequestMapping(value = "/employee/delete")
	public String hapus(@ModelAttribute Employee item) {
		
		//	mengirim item agar dapat di delete dari database
		repo.delete(item);
		
		return "redirect:/employee";
	}
	
}
