package com.xsis.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Biodata;
import com.xsis.demo.repository.BiodataRepo;

@Controller
public class BiodataController {

	//	membuat auto instance dari repository
	@Autowired
	private BiodataRepo repo;
	
	//	request halaman index yang ada di url localhost:port/biodata/index
	@RequestMapping("/biodata")
	public String index(Model model) {
		
		//	membuat object list biodata,
		//	kemudian diisi dari object repo dengan method findAll (mencari seluruh data)
		List<Biodata> data = repo.findAll();
		
		//	mengirim variable listData, value nya diisi dari object data
		model.addAttribute("listData", data);
		
		return "/biodata/index";
	}
	
	
	//	request halaman add yang ada di url localhost:port/biodata/add
	@RequestMapping(value="/biodata/add")
	public String add() {
		
		return "/biodata/add";
	}
	
	//	melakukan save data ke dalam model Biodata
	//	dengan menggunakan method post
	@RequestMapping(value="/biodata/save", method=RequestMethod.POST)
	public String save(@ModelAttribute Biodata item) {
		
		//	mengirim item agar dapat disave kedatabase
		repo.save(item);
		
		return "redirect:/biodata";
	}
	
	//	request edit data dengan berdasarkan id spesifik
	@RequestMapping(value="/biodata/edit/{id}")
	public String edit(Model model, @PathVariable(name="id") Integer id) {
		
		//	 mencari item yang mau di edit dari model Biodata berdasarkan id spesifik
		Biodata item = repo.findById(id).orElse(null);
		
		// mengirim variable data, value diisi dari object item
		model.addAttribute("data", item);
		
		return "biodata/edit";
	}	
	
	//	request delete data dengan berdasarkan id spesifik
	@RequestMapping(value = "/biodata/delete/{id}")
	public String hapus(@PathVariable(name = "id") Integer id) {
		
		//	mengambil data dari database dengan parameter id
		Biodata item = repo.findById(id).orElse(null);
		
		//	melakukan pengecekan pada item, apabila tidak null, maka lakukan delete
		if (item != null) {
			repo.delete(item);
		}
		
		return "redirect:/biodata";
	}
	
}
