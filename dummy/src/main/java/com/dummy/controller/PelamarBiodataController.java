package com.dummy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dummy.model.AddressModel;
import com.dummy.model.BiodataModel;
import com.dummy.repository.AddressRepo;
import com.dummy.repository.BiodataRepo;
import com.dummy.viewModel.BiodataForm;

@Controller
public class PelamarBiodataController {

	@Autowired
	private BiodataRepo repoBio;
	
	@Autowired
	private AddressRepo repoAddr;
	
	
	@RequestMapping(value="/pelamar/biodata", method=RequestMethod.GET)
	public String index(Model model, @PathVariable(name="id") Long id) {
		
		BiodataModel item = repoBio.findById(id).orElse(null);
		
		model.addAttribute("data", item);
		
		return "/pelamar/biodata/index";
	}
	
		
	@RequestMapping(value="/pelamar/biodata/save", method=RequestMethod.POST)
	public String saveToBiodata(@RequestBody BiodataForm model) {
		
		BiodataModel item = model.getBiodata();
		
		repoBio.save(item);

		AddressModel addr = model.getAddress();
		
		addr.setBiodataId(item.getId());
		
		repoAddr.save(addr);
		
		
		return "redirect:/pelamar/biodata";
	}
	
	
	
	
	@RequestMapping(value="/pelamar/biodata/edit/{id}", method=RequestMethod.GET)
	public String edit(Model model, @PathVariable(name="id") Long id) {
		
		BiodataModel item = repoBio.findById(id).orElse(null);
		
		model.addAttribute("data", item);
		
		return "/pelamar/biodata/edit";
	}
	
}
