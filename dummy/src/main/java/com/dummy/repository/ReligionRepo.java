package com.dummy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dummy.model.ReligionModel;

@Repository
public interface ReligionRepo extends JpaRepository<ReligionModel, Long> {

}
