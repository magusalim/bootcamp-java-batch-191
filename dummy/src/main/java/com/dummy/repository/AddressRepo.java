package com.dummy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dummy.model.AddressModel;

@Repository
public interface AddressRepo extends JpaRepository<AddressModel, Long> {

}
