package com.dummy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dummy.model.IdentityTypeModel;

@Repository
public interface IdentityTypeRepo extends JpaRepository<IdentityTypeModel, Long> {

}
